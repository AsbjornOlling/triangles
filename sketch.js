var width = window.innerWidth
var height = window.innerHeight


// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

function equilateral(a) {
  // equilateral triangle facing up
  //             centered on [0, 0]
  // a is side length
  // h is triangle height
  let h = a * (Math.sqrt(3) / 2)
  return [
    0,    -h * (2/3), // top
    -a/2, h/3,        // bottom left
    a/2,  h/3         // bottom right
  ]
}

// move a point or a triangle by v
let move_thing = (xys, v) => xys.map((e, i) => e + (i%2==0 ? v[0] : v[1]))

// rotate a point or a triangle around point c by theta
function rotate_thing(xys, c, th) {
  let at_origin = move_thing(xys, [-c[0], -c[1]])
  let rotated = at_origin.map((e, i, l) =>
    i%2 == 0
    ? e * cos(th) - l[i+1] * sin(th)  // xs
    : e * cos(th) + l[i-1] * sin(th)  // ys
  )
  return move_thing(rotated, c)
}

// ...I miss python
let range = ((n) => [...Array(n).keys()])

// duplicate thing horizontally to fill screen
let spaced_row = ((xys, sep) =>
  range(ceil(width/sep)+1).map(i => move_thing(xys, [sep*i, 0]))
)

// duplicate thing vertically to fill screen
let spaced_column = ((xys, sep) =>
  range(ceil(height/sep)+1).map(i => move_thing(xys, [0, sep*i]))
)

// a regular tiling of equlateral triangles
function equilateral_rows(triangle, size) {
  //let aligned = move_thing(triangle, [size/2, size/3])
  let column = spaced_column(triangle, size * sqrt(3)/2)
  let shifty_column = column.map((t, i) => i%2==0 ? t : move_thing(t, [-size/2, 0]))
  let rows = shifty_column.map(t => spaced_row(t, size))
  return rows
}

// middle point of a line segment
let segment_midpoint = ((p1, p2) => [(p1[0] + p2[0])/2, (p1[1] + p2[1])/2])


// midpoints for each edge segment of a triangle
function midpoints(triangle) {
  let a = [triangle[0], triangle[1]]
  let b = [triangle[2], triangle[3]]
  let c = [triangle[4], triangle[5]]

  let A = segment_midpoint(a, b)
  let B = segment_midpoint(b, c)
  let C = segment_midpoint(c, a)
  return [A, B, C]
}

// center point of an equilateral triangle
function triangle_center(t) {
  let base = segment_midpoint(
    [t[0], t[1]], [t[2], t[3]]
  )
  let tip = [t[4], t[5]]
  return [
    lerp(base[0], tip[0], 1/3), 
    lerp(base[1], tip[1], 1/3)
  ]
}

// centers of the three triangles adjacent to `t`
// in a regular tiling of equilateral triangles
function adjacent_centers(t) {
  let c = triangle_center(t)
  return midpoints(t).map(p => move_thing(p, [p[0] - c[0], p[1] - c[1]]))
}


var static_i = 0
function new_rotate_point(t, inverse) {
  let candidates = adjacent_centers(t)
  // candidates.push(triangle_center(t))
  static_i += 1
  // let randi = Math.floor(candidates.length * Math.random())
  return candidates[static_i % candidates.length]
}

// draw a triangle with p5js
function drawTriangle(t, c) {
  fill(c)
  noStroke()
  triangle(...t)
}
var rows

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)

  rows = equilateral_rows(platonic, size)
}


const size = 100
const platonic = equilateral(size)
const speed = 0.04

const dt = 0.005
var time = 0
var n = 0

var inverse = true
var tri = platonic
var rotate_point

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(60)
  rows = equilateral_rows(platonic, size)
  rotate_point = new_rotate_point(tri, inverse)
}

function draw() {
  const c1 = color(15, 48, 180)
  const c2 = color(15, 48, 200)
  let bg = inverse ? c1 : c2
  let c = inverse ? c2 : c1
  background(bg)

  // rotate to match time state
  var theta = lerp(0, 2*PI/3, n)
  console.log(tri)
  let rotated = rotate_thing(tri, rotate_point, theta)

  // make grid
  rows = equilateral_rows(rotated, size)
  let all = [].concat(...rows)

  // render
  all.map(t => drawTriangle(t, c))

  time += dt
  n += speed * (noise(time) - 0.5)

  // toggle tiling invert on completed rotation
  if (n < 0 || 1 < n) {
    n = n > 1 ? 0 : 1
    inverse = !inverse

    // pick a new triangle
    if (inverse) {
      tri = rotate_thing(platonic, [0, 0], PI)
      tri = move_thing(tri, [-size/2, (-size * sqrt(3)/2)/3])
    } else {
      tri = platonic
    }

    // pick a new rotate point
    rotate_point = new_rotate_point(tri)
  }

}
